class AllowedMoves:

    __up = "up"
    __down = "down"
    __left = "left"
    __right = "right"

    up = __up
    down = __down
    left = __left
    right = __right

    all_moves = [ __up, __down, __left, __right ]

