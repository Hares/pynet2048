import unittest
from game_2048.game import Game


class InRowMovesTestCase(unittest.TestCase):
    move_row_command = Game._Game__move_row

    def test_case_1(self):
        input = [ 3, 0, 0, 0, ]
        estimated_output = [ 3, 0, 0, 0, ]
        estimated_has_change = False
        estimated_score_change = 0

        actual_output, actual_has_change, actual_score_change = self.move_row_command(input)
    
        self.assertEqual(actual_output, estimated_output, "The resulting array does not match estimated")
        self.assertEqual(actual_has_change, estimated_has_change, "The resulting change detector does not match estimated")
        self.assertEqual(actual_score_change, estimated_score_change, "The resulting score change does not match estimated")

    def test_case_2(self):
        input = [ 0, 2, 2, 0, ]
        estimated_output = [ 3, 0, 0, 0, ]
        estimated_has_change = True
        estimated_score_change = 8

        actual_output, actual_has_change, actual_score_change = self.move_row_command(input)

        self.assertEqual(actual_output, estimated_output, "The resulting array does not match estimated")
        self.assertEqual(actual_has_change, estimated_has_change, "The resulting change detector does not match estimated")
        self.assertEqual(actual_score_change, estimated_score_change, "The resulting score change does not match estimated")

    def test_case_3(self):
        input = [ 2, 1, 0, 1, ]
        estimated_output = [ 2, 2, 0, 0, ]
        estimated_has_change = True
        estimated_score_change = 4
        
        actual_output, actual_has_change, actual_score_change = self.move_row_command(input)

        self.assertEqual(actual_output, estimated_output, "The resulting array does not match estimated")
        self.assertEqual(actual_has_change, estimated_has_change, "The resulting change detector does not match estimated")
        self.assertEqual(actual_score_change, estimated_score_change, "The resulting score change does not match estimated")

    def test_case_4(self):
        input = [ 0, 0, 4, 0, ]
        estimated_output = [ 4, 0, 0, 0, ]
        estimated_has_change = True
        estimated_score_change = 0
        
        actual_output, actual_has_change, actual_score_change = self.move_row_command(input)

        self.assertEqual(actual_output, estimated_output, "The resulting array does not match estimated")
        self.assertEqual(actual_has_change, estimated_has_change, "The resulting change detector does not match estimated")
        self.assertEqual(actual_score_change, estimated_score_change, "The resulting score change does not match estimated")

    def test_case_5(self):
        input = [ 2, 2, 2, 2, ]
        estimated_output = [ 3, 3, 0, 0, ]
        estimated_has_change = True
        estimated_score_change = 16
        
        actual_output, actual_has_change, actual_score_change = self.move_row_command(input)

        self.assertEqual(actual_output, estimated_output, "The resulting array does not match estimated")
        self.assertEqual(actual_has_change, estimated_has_change, "The resulting change detector does not match estimated")
        self.assertEqual(actual_score_change, estimated_score_change, "The resulting score change does not match estimated")

    def test_case_6(self):
        input = [ 2, 1, 1, 2, ]
        estimated_output = [ 2, 2, 2, 0, ]
        estimated_has_change = True
        estimated_score_change = 4
        
        actual_output, actual_has_change, actual_score_change = self.move_row_command(input)

        self.assertEqual(actual_output, estimated_output, "The resulting array does not match estimated")
        self.assertEqual(actual_has_change, estimated_has_change, "The resulting change detector does not match estimated")
        self.assertEqual(actual_score_change, estimated_score_change, "The resulting score change does not match estimated")

    def test_case_7(self):
        input = [ 1, 2, 1, 2, ]
        estimated_output = [ 1, 2, 1, 2, ]
        estimated_has_change = False
        estimated_score_change = 0
        
        actual_output, actual_has_change, actual_score_change = self.move_row_command(input)

        self.assertEqual(actual_output, estimated_output, "The resulting array does not match estimated")
        self.assertEqual(actual_has_change, estimated_has_change, "The resulting change detector does not match estimated")
        self.assertEqual(actual_score_change, estimated_score_change, "The resulting score change does not match estimated")

if __name__ == '__main__':
    unittest.main()
