import copy

from game_2048.allowed_moves import AllowedMoves
from game_2048.exceptions import *
from typing import Tuple, List
import random

class Game:

# private members:
    __state = None
    __score = 0

    def __init__(self, config='default'):
        config = \
        {
            'width': 4,
            'height': 4,
            'victory': 11,
            'starting_numbers': 2,
            'spawn_chances': { 1: 0.9, 2: 0.1 },
        }
        
        self.__width = config['width']
        self.__height = config['height']
        self.__victory = config['victory']
        self.__starting_numbers = config['starting_numbers']
        self.__spawn_chances = config['spawn_chances']
        
        self.__score = 0
        self.__state = [ [ 0 for i in range(self.__width) ] for j in range(self.__height) ]
        for _ in range(self.__starting_numbers):
            self.__spawn()


    def __spawn(self):
        _free = [ j*self.__width + i for i in range(self.__width) for j in range(self.__height) if (self.__state[j][i] == 0) ]
        # print(_free)
        if (len(_free) == 0):
            raise GameError ("Could not spawn more cells")
        
        _position = random.choice(_free)
        while(True):
            _value = random.choice(list(self.__spawn_chances.keys()))
            if (random.random() <= self.__spawn_chances[_value]):
                break
        
        self.__state[_position // self.__width][_position % self.__width] = _value

    def __move(self, direction:str) -> bool:
        _has_changes = False
        _score = 0
        
        if (direction == AllowedMoves.left):
            for j in range(self.__height):
                _row = self.__state[j]
                _row, _has_row_changes, _row_score = self.__move_row(_row)
                self.__state[j] = _row
                _has_changes = _has_changes or _has_row_changes
                _score += _row_score
        
        if (direction == AllowedMoves.right):
            for j in range(self.__height):
                _row = self.__state[j]
                _row = list(reversed(_row))
                _row, _has_row_changes, _row_score = self.__move_row(_row)
                _row = list(reversed(_row))
                self.__state[j] = _row
                _has_changes = _has_changes or _has_row_changes
                _score += _row_score
        
        if (direction == AllowedMoves.up):
            for i in range(self.__width):
                _row = [ self.__state[j][i] for j in range(self.__height) ]
                _row, _has_row_changes, _row_score = self.__move_row(_row)
                for j in range(self.__height):
                    self.__state[j][i] = _row[j]
                _has_changes = _has_changes or _has_row_changes
                _score += _row_score

        if (direction == AllowedMoves.down):
            for i in range(self.__width):
                _row = [ self.__state[j][i] for j in range(self.__height) ]
                _row = list(reversed(_row))
                _row, _has_row_changes, _row_score = self.__move_row(_row)
                _row = list(reversed(_row))
                for j in range(self.__height):
                    self.__state[j][i] = _row[j]
                _has_changes = _has_changes or _has_row_changes
                _score += _row_score

        self.__score += _score
        return _has_changes
        
    @classmethod
    def __dbg(cls, *args, **kwargs):
        pass
        # print(*args, **kwargs)
    
    @classmethod
    def __move_row(cls, row:List[int]) -> Tuple[List[int], bool, int]:
        _has_cell_protection = False
        _has_change = False
        _score_change = 0
        cls.__dbg("input")
        cls.__dbg(row)
        for i in range(1, len(row)):
            if (row[i] == 0):
                continue
            
            _moved = False
            for j in range(i, 0, -1):
                # go next
                if (row[j - 1] == 0):
                    continue
                
                # merge
                if (row[j - 1] == row[i]):
                    if (not _has_cell_protection):
                        row[i] = 0
                        row[j - 1] += 1
                        _score_change += 2 ** row[j - 1]
                        cls.__dbg("merge {0} and {1}".format(j - 1, i))
                        cls.__dbg(row)
                        _moved = True
                        _has_change = True
                        _has_cell_protection = True
                        break
                
                if (i == j):
                    break
                
                row[j], row[i] = row[i], row[j]
                cls.__dbg("swap {0} and {1}".format(j, i))
                cls.__dbg(row)
                _moved = True
                _has_change = True
                _has_cell_protection = False
                break
            
            if (not _moved and row[0] == 0):
                row[0], row[i] = row[i], row[0]
                cls.__dbg("swap {0} and {1}".format(0, i))
                cls.__dbg(row)
                _has_change = True
                _has_cell_protection = False

        return row, _has_change, _score_change
    
    def __check_defeat(self):
        _test = any(self.__state[j][i] == 0 for i in range(self.__width) for j in range(self.__height))
        if (_test):
            _state_backup = copy.deepcopy(self.__state)
            for _direction in (AllowedMoves.all_moves):
                if (self.__move(direction=_direction)):
                    self.__state = _state_backup
                    return False
            
            return True
        
        return False
    def __check_victory(self):
        return any(self.__state[j][i] >= self.__victory for i in range(self.__width) for j in range(self.__height))

    
    # public methods:
    @property
    def score(self):
        return self.__score
    
    @property
    def state(self):
        return copy.deepcopy(self.__state)
    
    def save(self):
        # TODO: Save state & RNG value
        pass

    def dumps(self) -> str:
        _format = "{:^5}"
        result = '\n'.join(' '.join(_format.format((2 ** self.__state[j][i]) if self.__state[j][i] else '0') for i in range(self.__width)) for j in range(self.__height))
        return result
    
    def load(self):
        # TODO: Load state & RNG value
        pass

    def move(self, command:str) -> bool:
        assert isinstance(command, str), "Command should be a string"

        command = command.lower().strip()
        assert command in AllowedMoves.all_moves, "Move '{command}' is not allowed".format(**locals())
        
        result = self.__move(command)
        if (not result):
            return False
        
        self.__spawn()
        if (self.__check_victory()):
            raise GameOverException("Victory")
        if (self.__check_defeat()):
            raise GameOverException("Game over")
        
        return True
    
    @staticmethod
    def play():
        game_obj = Game()
        print("Game started:")
        print(game_obj.dumps())
        print()
        
        while (True):
            _cmd = input("Make a move: ").lower().strip()
            
            # if (_cmd == 'spawn'):
            #     game_obj.__spawn()
            #     print(game_obj.dumps())
            #     print()
            #     continue

            try:
                _result = game_obj.move(_cmd)
            except AssertionError:
                print("Wrong command!")
            except GameOverException:
                print("Game over, try again.")
                break
            except GameVictoryException:
                print("Congratulations! You win!")
                break
            else:
                if (_result):
                    print("Your score is: {:}".format(game_obj.score))
                    print(game_obj.dumps())
                    print()
                    
                else:
                    print("Illegal move")

        
        print("Your total score is: {:}".format(game_obj.score))
        
if (__name__ == '__main__'):
    Game.play()
    