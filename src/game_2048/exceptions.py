class GameException(Exception):
    pass

class GameError(GameException):
    pass

class GameOverException(GameException):
    pass

class GameVictoryException(GameException):
    pass